# Système de magie

## Difficulté d'emploi

La magie est une chose compliquée et il faut la pratiquer plusieurs dizaines d'années avant de commencer à en comprendre le fonctionnement. Ainsi il est difficile pour un mage de connaître la difficulté de son sort avant de le mettre en oeuvre. 

## Consommation d'endurance

 * coût fixe déterminé par le MJ plus éventuellement une partie aléatoire dépendant de la situation
 * multiplicateur entre 0.5 (réussi) et 1.5 (échoué) déterminé par le jet du joueur

## Mode d'emploi

 * Le joueur doit 