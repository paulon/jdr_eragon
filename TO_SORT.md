# Fiche perso
## Caractéristiques
 * Force mentale (genre je sais faire un dudu de l'esprit)
 * Force physique (TAPEEEEER !)
 * Puissance magique (BOULE DE FEU SA MERE LA TEPU !) == (force ment + force phy)*modif racial
 * Agilité (parkour mdr)
 * Erudition (euh sé leu franssé)
 * Charisme (komen té boooo)
 * Artisanat (PT mdr)
 

# La magie

Pour lancer un sort un personnage doit :

 * Avoir une intention précise / une image claire de ce qu'il veut faire
    Jet en Force d'esprit (Mental) pour former l'image ou s'accrocher à l'intention.

 * Formuler une expression en Ancien Langage
    Jet en Ancien Langage (Erudition) qui jauge de l'exactitude de la langue et de la précision des mots

 * Mobiliser de l'énergie 
    Les effets rééls du sort (et non pas ceux escomptés par le joueur) vont demander une certaine squantité d'énergie qui devra être fournie. La formulation de l'incantation par le joueur déterminera s'il peut l'interrompre ou si le sort s'éxécutera jusqu'au bout ou jusqu'à la mort du magicien.
    