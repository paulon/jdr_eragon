# Statistiques
 
 Les statistiques sont découpés en 6 grands domaines :
## Mental

 * Affinité magique
 * Force d'esprit

## Artisanat

## Force phy

## Agilité

## Charisme

## Erudition

 * Ancien Langage
    Limite le nombre et la puissance des mots connus par le joueur et sa capacité à les utiliser dans une phrase. Représente sa difficulté à formuler une incantation valide et fidèle à son intention.

 * Lire Écrire
    Quantifie la capacité du joueur à lire des documents et produire des écrits.

 * Géographie
 * Histoire